# Install on Koompi
- Install python 3.9 (should be already installed, check with python --version)
- curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
- sudo python get-pip.py
- sudo pip install --compile --install-option=-O1 Pillow
- sudo pip install arcade
- python game.py

