import arcade
from board import *

SCREEN_WIDTH = 700
SCREEN_HEIGHT = 600
SCREEN_TITLE = "PNC Disk Game in Python"

class MyGame(arcade.Window):

    hoverCol = -1

    def __init__(self):
        # Call the parent class and set up the window
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        arcade.set_background_color(arcade.csscolor.CORNFLOWER_BLUE)

    def setup(self):
        """ Set up the game here. Call this function to restart the game. """
        createBoard()
        pass

    def on_mouse_press(self, x, y, button, modifiers):
        if ( getBoardStatus() == "IN_PROGRESS" ):
            colToDrop = int(x/100)
            print("You clicked column : " + str(colToDrop))
            if(canPlay(colToDrop) == True):
                dropPiece(colToDrop)
                if ( getBoardStatus() != "IN_PROGRESS" ):
                    print(getBoardStatus())
            else:
                print("invalid move")
        else:
            resetBoard()

    def on_mouse_motion(self, x, y, dx, dy):
        self.hoverCol = int(x/100)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Code to draw the screen goes here
        spacing = 60
        margin = 50
        radius = 40
        if ( getBoardStatus() == "IN_PROGRESS" ):
            for i in range(BOARD_ROW):
                for j in range(BOARD_COLUMN):
                    x = j * (spacing+radius) + margin
                    y = SCREEN_HEIGHT - ( i * (spacing+radius) + margin )
                    if(j == self.hoverCol):
                        arcade.draw_circle_outline(x, y, radius+5, arcade.color.BLACK, 5)
                    if(board[i][j] == " "):
                        arcade.draw_circle_filled(x, y, radius, arcade.color.AO)
                    elif(board[i][j] == "X"):
                        arcade.draw_circle_filled(x, y, radius, arcade.color.BANANA_YELLOW)
                    else:
                        arcade.draw_circle_filled(x, y, radius, arcade.color.AZURE_MIST)
        else:
            if(getBoardStatus() == "X_WIN"):
                arcade.draw_text("Yellow Player Wins", SCREEN_WIDTH/2 - 140, SCREEN_HEIGHT/2 - 40, arcade.color.BANANA_YELLOW, 40)
            elif(getBoardStatus() == "O_WIN"):
                arcade.draw_text("White Player Wins", SCREEN_WIDTH/2 - 140, SCREEN_HEIGHT/2 - 40, arcade.color.AZURE_MIST, 40)
            else:
                arcade.draw_text("Draw. Nobody wins.", SCREEN_WIDTH/2 - 140, SCREEN_HEIGHT/2 - 40, arcade.color.AZURE_MIST, 40)
            arcade.draw_text("Click anywhere to restart", SCREEN_WIDTH/2 - 140, SCREEN_HEIGHT/2 - 80, arcade.color.AZURE_MIST, 20)

def main():
    """ Main method """
    window = MyGame()
    window.setup()
    arcade.run()

if __name__ == "__main__":
    main()