BOARD_SIZE = 10

# print the grid with Balook inside
def printBoard(captainPosition):
    boardString = ""
    for rowIndex in range(BOARD_SIZE):
        for colIndex in range(BOARD_SIZE):
            if(captainPosition[0] == rowIndex and captainPosition[1] == colIndex):
                boardString += " ★"
            else:
                boardString += " 0"
        boardString += "\n"
    print(boardString)

# captain position is [rowIndex, columnIndex], he start at 0,0
captainPosition = [0,0]

#print board
printBoard(captainPosition)

# let the player plays 10 times
for actionIndex in range(10):

    # Ask player to input action R (right) or L (left) or U (up) or D (down)
    actionsString = input(">Input action number "+str(actionIndex)+" (R, L, U, D) : ")

    # move Right
    if(actionsString.upper() == "R"):
        captainPosition[1] += 1
    
    # print board again to see Balook position
    printBoard(captainPosition)


