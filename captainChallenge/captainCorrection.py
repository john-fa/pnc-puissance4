BOARD_SIZE = 10

# print the grid with Balook inside
def printBoard(captainPosition, enemyPosition):
    boardString = ""
    for rowIndex in range(BOARD_SIZE):
        for colIndex in range(BOARD_SIZE):
            if captainPosition[0] == rowIndex and captainPosition[1] == colIndex:
                boardString += " ★"
            elif enemyPosition[0] == rowIndex and enemyPosition[1] == colIndex:
                boardString += " R"
            else:
                boardString += " 0"
        boardString += "\n"
    print(boardString)

# captain position is [rowIndex, columnIndex], he start at 0,0
captainPosition = [0,0]

# enemy position is [rowIndex, columnIndex], he start at 0,0
enemyPosition = [5,5]

#print board
printBoard(captainPosition, enemyPosition)

# let the player plays until he finds the enemy
while captainPosition[0] != enemyPosition[0] or captainPosition[1] != enemyPosition[1]:

    # Ask player to input action R (right) or L (left) or U (up) or D (down)
    actionsString = input("> Input action (R, L, U, D) : ")

    # move Right
    if(actionsString.upper() == "R" and captainPosition[1] < BOARD_SIZE-1):
        captainPosition[1] += 1
    
    # move Left
    if(actionsString.upper() == "L" and captainPosition[1] > 0):
        captainPosition[1] -= 1

    # move Down
    if(actionsString.upper() == "D" and captainPosition[0] < BOARD_SIZE-1):
        captainPosition[0] += 1

    # move Up
    if(actionsString.upper() == "U" and captainPosition[0] > 0):
        captainPosition[0] -= 1
    
    # print board again to see Balook position
    printBoard(captainPosition, enemyPosition)

print("Captain wins !")


