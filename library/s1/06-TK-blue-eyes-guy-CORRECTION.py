import tkinter as tk

# Create an empty window
root = tk.Tk() 
# Set TK window size to width 600 px and height 200 px
root.geometry("600x600")
# Create a frame in the window (frame is a container, like "div" in HTML)
frame = tk.Frame() 
# Set the title of the frame
frame.master.title("Hello PNC")

# HERE YOU CAN START TO DRAW
# canvas is like "svg tag" in HTML, it allows user to draw shapes
canvas = tk.Canvas(frame)
canvas.create_oval(100, 30, 200, 80, fill="#FFFFFF")
canvas.create_oval(125, 30, 175, 80, fill="#0000FF")
canvas.create_oval(400, 30, 500, 80, fill="#FFFFFF")
canvas.create_oval(425, 30, 475, 80, fill="#0000FF")
canvas.create_oval(275, 200, 325, 250, fill="#FF0000")
canvas.create_rectangle(100, 400, 500, 420, fill="#FF0000", outline="#FF0000")
canvas.create_rectangle(100, 350, 120, 400, fill="#FF0000", outline="#FF0000")
canvas.create_rectangle(480, 380, 500, 400, fill="#FF0000", outline="#FF0000")
# pack means "draw what i put inside"
canvas.pack(expand=True, fill='both')
frame.pack(expand=True, fill='both')

# Display all
root.mainloop()