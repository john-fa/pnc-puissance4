import tkinter as tk
import random

# Create an empty window
root = tk.Tk() 
# Set TK window size to width 600 px and height 200 px
root.geometry("600x600")
# Create a frame in the window (frame is a container, like "div" in HTML)
frame = tk.Frame() 
# Set the title of the frame
frame.master.title("Hello PNC")
canvas = tk.Canvas(frame)

for row in range(0,5):
    for column in range(0,5):
        marginX = column * 10
        marginY = row * 10
        x1 = 10 + (column * 50) + marginX
        y1 = 10 + (row * 50) + marginY
        x2 = x1 + 50
        y2 = y1 + 50
        if row == column:
            canvas.create_oval(x1, y1, x2, y2, fill="red")
        else:
            canvas.create_oval(x1, y1, x2, y2, fill="blue")

# pack means "draw what i put inside"
canvas.pack(expand=True, fill='both')
frame.pack(expand=True, fill='both')

# Display all
root.mainloop()