BOARD_ROW = 6
BOARD_COLUMN = 7
BOARD_STATUS = 'IN_PROGRESS'
board = []
turn = 0
signs = ['X', 'O']

def createBoard():
    for i in range(BOARD_ROW):
        board.append([' '] * BOARD_COLUMN)
    return board

def resetBoard():
    global BOARD_STATUS
    BOARD_STATUS = 'IN_PROGRESS'
    for i in range(BOARD_ROW):
        for j in range(BOARD_COLUMN):
            board[i][j] = ' '

def canPlay(col):
    if(col < BOARD_COLUMN):
        for row in range(BOARD_ROW):
            if(board[row][col] == ' '):
                return True
    return False

def dropPiece(col):
    global turn
    sign = signs[turn%2]
    if(canPlay(col)):
        for row in range(BOARD_ROW):
            if(board[row][col] != ' '):
                board[row-1][col] = sign
                turn = turn + 1
                winCheck(sign, row-1, col)
                return True
            elif(row == BOARD_ROW - 1):
                board[row][col] = sign
                turn = turn + 1
                winCheck(sign, row, col)
                return True
    return False

def isFull():
    for col in range(BOARD_COLUMN):
        if(board[0][col] == ' '):
            return False
    return True

def getBoardStatus():
    return BOARD_STATUS

def winCheck(sign, row, col):
    global BOARD_STATUS
    row_min = row - 3 if ( row - 3 >= 0 ) else 0
    row_max = row + 3 if ( row + 3 < BOARD_ROW ) else BOARD_ROW - 1
    col_min = col - 3 if ( col - 3 >= 0 ) else 0
    col_max = col + 3 if ( col + 3 < BOARD_COLUMN ) else BOARD_COLUMN - 1

    #HORIZONTAL CHECK
    consecutive_sign = 0
    for i in range(col_min,col_max+1):
        if board[row][i] == sign:
            consecutive_sign+=1
            if (consecutive_sign == 4):
                BOARD_STATUS = sign + "_WIN"
                return True
        else:
            consecutive_sign=0

    #VERTICAL CHECK
    consecutive_sign = 0
    for i in range(row_min,row_max+1):
        if board[i][col] == sign:
            consecutive_sign+=1
            if (consecutive_sign == 4):
                BOARD_STATUS = sign + "_WIN"
                return True
        else:
            consecutive_sign=0

    # DIAGONAL CHECK
    #    first diagonal : \
    consecutive_sign = 0
    row_min = row_max = row
    col_min = col_max = col
    while(row_min > 0 and col_min > 0):
        row_min -= 1
        col_min -= 1
    while(row_max < BOARD_ROW-1 and col_max < BOARD_COLUMN-1):
        row_max += 1
        col_max += 1
    x = row_min
    y = col_min
    while(x <= row_max and y <= col_max):
        if board[x][y] == sign:
            consecutive_sign+=1
        else:
            consecutive_sign = 0
        if (consecutive_sign == 4):
            BOARD_STATUS = sign + "_WIN"
            return True
        x += 1
        y += 1
    #     second diagonal : /
    consecutive_sign = 0
    row_min = row_max = row
    col_min = col_max = col
    while(row_min > 0 and col_min < BOARD_COLUMN-1):
        row_min -= 1
        col_min += 1
    while(row_max < BOARD_ROW-1 and col_max > 0):
        row_max += 1
        col_max -= 1
    x = row_min
    y = col_min
    while(x <= row_max and y >= 0):
        if board[x][y] == sign:
            consecutive_sign+=1
        else:
            consecutive_sign = 0
        if (consecutive_sign == 4):
            BOARD_STATUS = sign + "_WIN"
            return True
        x += 1
        y -= 1

    if(BOARD_STATUS == "IN_PROGRESS"):
        if(isFull()):
            BOARD_STATUS = "FULL"

    return False