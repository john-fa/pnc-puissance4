import tkinter as tk
from tkinter import messagebox

def fileToGrid():
    global grid
    grid = [[]]
    labFile = open("labyrinth.txt", "r")
    i = j = 0
    for line in labFile:
        for character in line:
            if character == "\n":
                i = i + 1
                grid.append([])
            else:
                grid[i].append(character)
    return grid

def checkWin(x,y):
    if grid[y][x] == "T":
        print("You win!")
        messagebox.showinfo("SUCCESS", "You win !")

def moveUp(evt):
    global grid, win, playerX, playerY,canvas,player
    if playerY>0 and grid[playerY-1][playerX] != "1":
        checkWin(playerX,playerY-1)
        grid[playerY-1][playerX] = "P"
        grid[playerY][playerX] = "0"
        playerY = playerY - 1
        canvas.move(player, 0, -1*SQUARE_SIZE)
    
def moveDown(evt):
    global grid, win, playerX, playerY,canvas,player
    if playerY<len(grid)-1 and grid[playerY+1][playerX] != "1":
        checkWin(playerX,playerY+1)
        grid[playerY+1][playerX] = "P"
        grid[playerY][playerX] = "0"
        playerY = playerY + 1
        canvas.move(player, 0, SQUARE_SIZE)

def moveLeft(evt):
    global grid, win, playerX, playerY,canvas,player
    if playerX>0 and grid[playerY][playerX-1] != "1":
        checkWin(playerX-1,playerY)
        grid[playerY][playerX-1] = "P"
        grid[playerY][playerX] = "0"
        playerX = playerX - 1
        canvas.move(player, -1*SQUARE_SIZE, 0)

def moveRight(evt):
    global grid, win, playerX, playerY,canvas,player
    if playerX<len(grid[playerY])-1 and grid[playerY][playerX+1] != "1":
        checkWin(playerX+1,playerY)
        grid[playerY][playerX+1] = "P"
        grid[playerY][playerX] = "0"
        playerX = playerX + 1
        canvas.move(player, SQUARE_SIZE, 0)

def drawBoard():
    global player
    y = 0
    if win == False:
        for line in grid:
            y = y + 1
            x = 0
            for cell in line:
                x = x + 1
                if cell == "1":
                    canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#2c3e50", fill="#2c3e50")
                elif cell == "0" or cell == "P":
                    canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#27ae60", fill="#27ae60")
                elif cell == "T":
                    canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#f1c40f", fill="#f1c40f")                        
                if cell == "P":
                    player = canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#1abc9c", fill="#1abc9c", tags="player")
    canvas.tag_raise(player) # put player on the foreground
    canvas.pack(expand=True, fill='both')

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = "PNC Labrinth Game in Python"
SQUARE_SIZE = 35
player = 0
playerX = playerY = canvas = 0
win = False

root = tk.Tk()
root.geometry(str(SCREEN_WIDTH)+"x"+str(SCREEN_HEIGHT))
grid = fileToGrid()
frame = tk.Frame()
frame.master.title("Labyrinth in TK")
frame.pack(expand=True, fill='both')
canvas = tk.Canvas(frame)
root.bind("<Up>",moveUp)
root.bind("<Down>",moveDown)
root.bind("<Right>",moveRight)
root.bind("<Left>",moveLeft)
drawBoard()
root.mainloop()