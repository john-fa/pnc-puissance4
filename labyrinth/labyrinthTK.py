from tkinter import Tk, Canvas, Frame, BOTH
from tkinter import messagebox as mbox

def fileToGrid():
    global grid
    grid = [[]]
    labFile = open("labyrinth.txt", "r")
    i = j = 0
    for line in labFile:
        for character in line:
            if character == "\n":
                i = i + 1
                grid.append([])
            else:
                grid[i].append(character)
    return grid

def checkWin(x,y):
    global win
    if grid[y][x] == "T":
        win=True

def moveUp(evt):
    global grid, win, playerX, playerY
    if playerY>0 and grid[playerY-1][playerX] != "1":
        checkWin(playerX,playerY-1)
        grid[playerY-1][playerX] = "P"
        grid[playerY][playerX] = "0"
        playerY = playerY - 1
        ex.drawBoard()
    
def moveDown(evt):
    global grid, win, playerX, playerY
    if playerY<len(grid)-1 and grid[playerY+1][playerX] != "1":
        checkWin(playerX,playerY+1)
        grid[playerY+1][playerX] = "P"
        grid[playerY][playerX] = "0"
        playerY = playerY + 1
        ex.drawBoard()

def moveLeft(evt):
    global grid, win, playerX, playerY
    if playerX>0 and grid[playerY][playerX-1] != "1":
        checkWin(playerX-1,playerY)
        grid[playerY][playerX-1] = "P"
        grid[playerY][playerX] = "0"
        playerX = playerX - 1
        ex.drawBoard()

def moveRight(evt):
    global grid, win, playerX, playerY
    if playerX<len(grid[playerY])-1 and grid[playerY][playerX+1] != "1":
        checkWin(playerX+1,playerY)
        grid[playerY][playerX+1] = "P"
        grid[playerY][playerX] = "0"
        playerX = playerX + 1
        ex.drawBoard()

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = "PNC Labrinth Game in Python"
SQUARE_SIZE = 35
grid = fileToGrid()
playerX = playerY = 0
win = False
ex = 0

class Example(Frame):

    canvas = 0

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.master.title("Labyrinth in TK")
        self.pack(fill=BOTH, expand=1)
        self.canvas = Canvas(self)
        self.canvas.after(100, self.drawBoard)
    
    def drawBoard(self):
        y = 0
        if win == False:
            for line in grid:
                y = y + 1
                x = 0
                for cell in line:
                    x = x + 1
                    if cell == "1":
                        self.canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#2c3e50", fill="#2c3e50")
                    elif cell == "0":
                        self.canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#27ae60", fill="#27ae60")
                    elif cell == "P":
                        self.canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#1abc9c", fill="#1abc9c")
                    elif cell == "T":
                        self.canvas.create_rectangle(x * SQUARE_SIZE, y * SQUARE_SIZE, x * SQUARE_SIZE + SQUARE_SIZE, y * SQUARE_SIZE + SQUARE_SIZE, outline="#f1c40f", fill="#f1c40f")                        
        else:
            mbox.showinfo("SUCCESS", "You win !")
        self.canvas.pack(fill=BOTH, expand=1)
        print(playerX, playerY)
        # redram every 50 ms : destroy performance
        # self.canvas.after(50, self.drawBoard)

def main():
    global ex
    root = Tk()
    ex = Example()
    root.bind("<Up>",moveUp)
    root.bind("<Down>",moveDown)
    root.bind("<Right>",moveRight)
    root.bind("<Left>",moveLeft)
    root.geometry(str(SCREEN_WIDTH)+"x"+str(SCREEN_HEIGHT)+"+300+300")
    root.mainloop()


if __name__ == '__main__':
    main()