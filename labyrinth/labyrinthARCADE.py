import arcade

def fileToGrid():
    global grid
    grid = [[]]
    labFile = open("labyrinth.txt", "r")
    i = j = 0
    for line in labFile:
        for character in line:
            if character == "\n":
                i = i + 1
                grid.append([])
            else:
                grid[i].append(character)
    return grid

def checkWin(x,y):
    global win
    if grid[y][x] == "T":
        win=True

def move(key):
    x=y=playerX=playerY=0
    global grid, win
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            if grid[y][x] == "P":
                playerX = x
                playerY = y
    if key == arcade.key.UP and playerY>0 and grid[playerY-1][playerX] != "1":
        checkWin(playerX,playerY-1)
        grid[playerY-1][playerX] = "P"
        grid[playerY][playerX] = "0"
    elif key == arcade.key.DOWN and playerY<len(grid)-1 and grid[playerY+1][playerX] != "1":
        checkWin(playerX,playerY+1)
        grid[playerY+1][playerX] = "P"
        grid[playerY][playerX] = "0"
    elif key == arcade.key.LEFT and playerX>0 and grid[playerY][playerX-1] != "1":
        checkWin(playerX-1,playerY)
        grid[playerY][playerX-1] = "P"
        grid[playerY][playerX] = "0"
    elif key == arcade.key.RIGHT and playerX<len(grid[playerY])-1 and grid[playerY][playerX+1] != "1":
        checkWin(playerX+1,playerY)
        grid[playerY][playerX+1] = "P"
        grid[playerY][playerX] = "0"

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = "PNC Labrinth Game in Python"
SQUARE_SIZE = 35
grid = [[]]
win = False

class MyGame(arcade.Window):

    player_sprite = 0

    def __init__(self):
        # Call the parent class and set up the window
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        self.player_sprite = arcade.Sprite(":resources:images/animated_characters/female_person/femalePerson_idle.png", 0.5)
        self.player_sprite.center_x = 0
        self.player_sprite.center_y = 0
        arcade.set_background_color((44, 62, 80))

    def setup(self):
        """ Set up the game here. Call this function to restart the game. """
        global grid
        grid = fileToGrid()
        pass

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        move(key)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        y = 0
        
        if win == False:
            for line in grid:
                y = y + 1
                x = 0
                for cell in line:
                    x = x + 1
                    if cell == "1":
                        arcade.draw_rectangle_filled(x*SQUARE_SIZE, SCREEN_HEIGHT - y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE, (44, 62, 80))
                    elif cell == "0":
                        arcade.draw_rectangle_filled(x*SQUARE_SIZE, SCREEN_HEIGHT - y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE, (39, 174, 96))
                    elif cell == "P":
                        arcade.draw_rectangle_filled(x*SQUARE_SIZE, SCREEN_HEIGHT - y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE, (26, 188, 156))                       
                        self.player_sprite.center_x = x*SQUARE_SIZE
                        self.player_sprite.center_y = SCREEN_HEIGHT - y*SQUARE_SIZE + 10
                        self.player_sprite.draw()
                    elif cell == "T":
                        arcade.draw_rectangle_filled(x*SQUARE_SIZE, SCREEN_HEIGHT - y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE, (241, 196, 15))
        else:
            arcade.draw_text("You win !", SCREEN_WIDTH/2 - 140, SCREEN_HEIGHT/2 - 40, arcade.color.AZURE_MIST, 40) 

def main():
    """ Main method """
    window = MyGame()
    window.setup()
    arcade.run()

if __name__ == "__main__":
    main()
