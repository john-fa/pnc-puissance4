from board import *

def printBoard(): 
    for i in range(BOARD_ROW):
        for j in range(BOARD_COLUMN):
            print(board[i][j] , end=' | ')
        print()
    print('____'*BOARD_COLUMN)
    print()
    
print('WELCOME TO DISK-BATTLE!')
createBoard()
printBoard()
turn = 0
signs = ['X', 'O']
while(getBoardStatus() == "IN_PROGRESS"):
    colToDrop = int(input("Turn "+str(turn)+" / Player "+signs[turn%2]+" / input a column number : "))
    while(canPlay(colToDrop) == False):
        print("your move is invalid, try again")
        colToDrop = int(input("Turn "+str(turn)+" / Player "+signs[turn%2]+" / input a column number : "))
    dropPiece(colToDrop, signs[turn%2])
    printBoard()
    turn += 1

print("Board status : "+getBoardStatus())